//for map usage
mapboxgl.accessToken = 'pk.eyJ1IjoiYWFyb25mcmVkIiwiYSI6ImNrZGtsbzMzZjBweXIydG50dHlicmt6YXgifQ.X3BWk27FF7qyMEVpodGc7Q';
var map = new mapboxgl.Map({container: 'map', style: 'mapbox://styles/mapbox/streets-v10'});
var nav = new mapboxgl.NavigationControl({showCompass: false, visualizePitch: true});
	map.addControl(nav, 'top-left');
var markerHeight = 30, markerRadius = 5, linearOffset = 15;
var popupOffsets = {
	'top': [0, 0],
	'top-left': [0,0],
	'top-right': [0,0],
	'bottom': [0, -markerHeight],
	'bottom-left': [linearOffset, (markerHeight - markerRadius + linearOffset) * -1],
	'bottom-right': [-linearOffset, (markerHeight - markerRadius + linearOffset) * -1],
	'left': [markerRadius, (markerHeight - markerRadius) * -1],
	'right': [-markerRadius, (markerHeight - markerRadius) * -1]
};
var marker = new mapboxgl.Marker()
var popup = new mapboxgl.Popup();

//getting the index from localStorage
let index = JSON.parse(localStorage.getItem(TRIP_INDEX_KEY));

//retrieving the trip details
let trip_data = trips.getTrip(index);

//sleep function
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

//focus function
async function Focus (a, b)
{
	let f_max_lon = 0, f_max_lat = 0, f_min_lon = 0, f_min_lat = 0;
	if (a[0]>b[0])
	{
		f_max_lon = a[0];
		f_min_lon = b[0];
	}
	else
	{
		f_max_lon = b[0];
		f_min_lon = a[0];
	}
	if (a[1]>b[1])
	{
		f_max_lat = a[1];
		f_min_lat = b[1];
	}
	else
	{
		f_max_lat = b[1];
		f_min_lat = a[1];
	}

	map.fitBounds([[f_min_lon, f_min_lat], [f_max_lon, f_max_lat]], {padding: 40, linear: true, duration: 700});

	await sleep(3000);

	map.fitBounds([[min_lon, min_lat], [max_lon, max_lat]], {padding: 50, linear: true, duration: 500});
}

let max_lon = parseFloat(trip_data.airports[0]["longitude"]), min_lon = parseFloat(trip_data.airports[0]["longitude"]), max_lat = parseFloat(trip_data.airports[0]["latitude"]), min_lat = parseFloat(trip_data.airports[0]["latitude"]);

let line = []
for (i=0; i<trip_data.airports.length; i++)
{
	let lon	= parseFloat(trip_data.airports[i]["longitude"]);
	let lat = parseFloat(trip_data.airports[i]["latitude"]);

	line.push([lon, lat]);

	if (lon > max_lon)
	{
		max_lon = lon;
	}
	if (lon < min_lon)
	{
		min_lon = lon;
	}
	if (lat > max_lat)
	{
		max_lat = lat;
	}
	if (lat < min_lat)
	{
		min_lat = lat;
	}

	let clr = "";
	if (i == 0)
	{
		clr = "#00ff00";
	}
	else if (i == trip_data.airports.length - 1)
	{
		clr = "#ff0000";
	}
	else
	{
		clr = "#0000ff";
	}

	popup = new mapboxgl.Popup({offset: popupOffsets, closeButton: false, closeOnClick: false}).setHTML(trip_data.airports[i]["IATA-FAA"]);

	marker = new mapboxgl.Marker({
			color: clr,
			draggable: false
		})
		.setLngLat([lon, lat])
		.setPopup(popup)
		.addTo(map);

	marker.togglePopup();

	let popup_container = document.getElementsByClassName("mapboxgl-popup-content");

	for (div of popup_container)
	{
		div.className = "mapboxglpopupcontent";
	}
}

map.on('load', function () {
	map.addSource('r', {
		'type': 'geojson',
		'data': {
			'type': 'Feature',
			'properties': {},
			'geometry': {
				'type': 'LineString',
				'coordinates': line
			}
		}
	});
	map.addLayer({
		'id': 'r',
		'type': 'line',
		'source': 'r',
		'layout': {
			'line-join': 'round',
			'line-cap': 'round'
		},
		'paint': {
			'line-color': '#ff1f1f',
			'line-width': 3
		}
	});
});

//focusing on to the markers area
map.fitBounds([[min_lon, min_lat], [max_lon, max_lat]], {padding: 50});

let output = "";
for (i=0;i<trip_data.route.length;i++)
{
	let cs = trip_data.route[i]["codeshare"];
	if (cs == "Y")
	{
		cs = "Code shared";
	}
	else 
	{
		cs = "No code shared";
	}

	output += `
	<div onclick='Focus([${trip_data.airports[i]["longitude"]},${trip_data.airports[i]["latitude"]}],[${trip_data.airports[i+1]["longitude"]},${trip_data.airports[i+1]["latitude"]}]);' class="route">
		<div class="head">${trip_data.airports[i]["name"]} to ${trip_data.airports[i+1]["name"]}</div>
		<div class="sub-head">${trip_data.airports[i]["IATA-FAA"]} > ${trip_data.airports[i+1]["IATA-FAA"]}</div>
		<div class="vr"></div>
		<div class="info">
			Airline: ${trip_data.route[i]["airline"]}<br>
			Airline ID: ${trip_data.route[i]["airlineId"]}<br>
			Equipment: ${trip_data.route[i]["equipment"]}<br>
			Stops: ${trip_data.route[i]["stops"]}<br>
			${cs}
		</div>
	</div>
	<hr>`
}
output = output.substring(0,output.length-4);

document.getElementById("view-info").innerHTML = output;