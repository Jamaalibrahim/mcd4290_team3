//for map usage
mapboxgl.accessToken = 'pk.eyJ1IjoiYWFyb25mcmVkIiwiYSI6ImNrZGtsbzMzZjBweXIydG50dHlicmt6YXgifQ.X3BWk27FF7qyMEVpodGc7Q';
var map = new mapboxgl.Map({container: 'map'});
var nav = new mapboxgl.NavigationControl({showCompass: false, visualizePitch: true});
	map.addControl(nav, 'top-left');
var markerHeight = 30, markerRadius = 5, linearOffset = 15;
var popupOffsets = {
	'top': [0, 0],
	'top-left': [0,0],
	'top-right': [0,0],
	'bottom': [0, -markerHeight],
	'bottom-left': [linearOffset, (markerHeight - markerRadius + linearOffset) * -1],
	'bottom-right': [-linearOffset, (markerHeight - markerRadius + linearOffset) * -1],
	'left': [markerRadius, (markerHeight - markerRadius) * -1],
	'right': [-markerRadius, (markerHeight - markerRadius) * -1]
};
var marker = new mapboxgl.Marker()
var popup = new mapboxgl.Popup();

//for trip usage
let Start = [];
let _Date = "";
let Airports_Array = [];

//path being selected (holds an array of obects where its properties are the airports and route information)
let path = []; 

//used in drawing lines for available routes
let line_for_map = [];

//used in funtions Airport_Select, Routes and Select_followup
let airport_select_i = -1 , airport_select = [];

function CONFIRM(i, d)
{
	let stops = i.length-2;
	let route = [];
	let airports = [];
	for (x of i)
	{
		if (x != i[0])
		{
			route.push(x["route"]);
			stops += x["route"]["stops"];
		}
		airports.push(x["airport"]);
	}
	trips.addTrip(airports, route, stops, d);

	updateLocalStorage(trips);

	window.location = "home.html";
}

//Select_followup function runs after the api request in function Select
function Select_followup(routes)
{
	document.getElementById("select-button").disabled = true;
	document.getElementById("select-button").className = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored disable";

	map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 3,
            center: [path[path.length-1]["airport"]["longitude"], path[path.length-1]["airport"]["latitude"]]
        });
	
	map.addControl(nav, 'top-left');

	document.getElementById("IATA-FAA").innerText = path[path.length-1]["airport"]["IATA-FAA"];
	document.getElementById("airport-name").innerText = path[path.length-1]["airport"]["name"];
	document.getElementById("airport-city").innerText = path[path.length-1]["airport"]["city"];
	document.getElementById("airport-country").innerText = path[path.length-1]["airport"]["country"];

	document.getElementById("route-info").innerHTML = `
	Airline: ${path[path.length-1]["route"]["airline"]}<br>
	No. of stops: ${path[path.length-1]["route"]["stops"]}<br>
	Equipment: ${path[path.length-1]["route"]["equipment"]}`;

	airport_select = [];
	for (route of routes)
	{
		for (ap of Airports_Array)
		{
			if (ap["airportId"] == route["destinationAirportId"])
			{
				airport_select.push({"route":route, "airport":ap});
				line_for_map.push([ap["longitude"], ap["latitude"]]);	

				marker = new mapboxgl.Marker({
					color: "#ffff00",
					draggable: false
				}).setLngLat([ap["longitude"], ap["latitude"]])
				.setPopup(new mapboxgl.Popup({offset: popupOffsets, closeButton: false, closeOnClick: false}).setHTML(ap["IATA-FAA"]))
				.addTo(map);

				//marker.togglePopup();

			}
		}
	}

	for (p=0; p<path.length; p++)
	{
		popup = new mapboxgl.Popup({offset: popupOffsets, closeButton: false, closeOnClick: false}).setHTML(path[p]["airport"]["IATA-FAA"]);

		let clr = "";
		if (p == 0)
		{
			clr = "#00ff00";
		}
		else if (p == path.length-1)
		{
			clr = "#ff0000";
		}
		else
		{
			clr = "#0000ff";
		}

		marker = new mapboxgl.Marker({
			color: clr,
			draggable: false
		})
		.setLngLat([path[p]["airport"]["longitude"], path[p]["airport"]["latitude"]])
		.setPopup(popup)
		.addTo(map);

		marker.togglePopup();
	}

	//adding lines between markers
	map.on('load', function () {
		s = 0
		for (line of line_for_map)
		{
			map.addSource(s.toString(10), {
				'type': 'geojson',
				'data': {
					'type': 'Feature',
					'properties': {},
					'geometry': {
						'type': 'LineString',
						'coordinates': [[path[path.length-1]["airport"]["longitude"], path[path.length-1]["airport"]["latitude"]], line]
					}
				}
			});
			map.addLayer({
				'id': s.toString(10),
				'type': 'line',
				'source': s.toString(10),
				'layout': {
					'line-join': 'round',
					'line-cap': 'round'
				},
				'paint': {
					'line-color': '#888',
					'line-width': 3
				}
			});

			s++;
		}

		let redpath = []
		for (p of path)
		{
			redpath.push([p["airport"]["longitude"], p["airport"]["latitude"]]);
		}
		map.addSource(s.toString(10), {
			'type': 'geojson',
			'data': {
				'type': 'Feature',
				'properties': {},
				'geometry': {
					'type': 'LineString',
					'coordinates': redpath
				}
			}
		});
		map.addLayer({
			'id': s.toString(10),
			'type': 'line',
			'source': s.toString(10),
			'layout': {
				'line-join': 'round',
				'line-cap': 'round'
			},
			'paint': {
				'line-color': '#ff1f1f',
				'line-width': 4
			}
		});
	});

	let popup_container = document.getElementsByClassName("mapboxgl-popup-content");

	for (div of popup_container)
	{
		div.className = "mapboxglpopupcontent";
	}
}

//Select functions runs when select button is clicked
function Select(a, r)
{
	path.push({"airport": a, "route": r})

	document.getElementById("confirm-button").disabled = false;
	document.getElementById("confirm-button").className = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent active";
	document.getElementById("confirm-button").onclick = function(){CONFIRM(path, _Date)};

	webServiceRequest("https://eng1003.monash/OpenFlights/routes/",{"sourceAirport":route["destinationAirportId"], "callback":"Select_followup"});

}

function Airport_Select(n)
{
	airport_select_i += n;
	if (airport_select_i < 0)
	{
		airport_select_i = airport_select.length-1;
	}
	if (airport_select_i >= airport_select.length)
	{
		airport_select_i = 0;
	}
	aps = airport_select[airport_select_i];
	document.getElementById("IATA-FAA").innerText = aps["airport"]["IATA-FAA"];
	document.getElementById("airport-name").innerText = aps["airport"]["name"];
	document.getElementById("airport-city").innerText = aps["airport"]["city"];
	document.getElementById("airport-country").innerText = aps["airport"]["country"];

	document.getElementById("route-info").innerHTML = `
	Airline: ${aps["route"]["airline"]}<br>
	No. of stops: ${aps["route"]["stops"]}<br>
	Equipment: ${aps["route"]["equipment"]}`;

	map.panTo([aps["airport"]["longitude"], aps["airport"]["latitude"]]);

	document.getElementById("select-button").disabled = false;
	document.getElementById("select-button").className = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored active";
	document.getElementById("select-button").onclick = function(){Select(aps["airport"], aps["route"])};
}

//serach bar drop down function
function AirportResponse(AirportArray)
{	
	let AirportDropdown = document.getElementById("airport_list");
	for (airport of AirportArray) {
		Airports_Array.push(airport);
		
		let Option = document.createElement("option");
		//Option.value = airport.airportId;
		Option.value = `${airport.country}, ${airport.city}, ${airport.name}`;
		if (airport["IATA-FAA"] != "") {
			Option.value += ` (${airport["IATA-FAA"]})`;
		}
		Option.value += `, ${airport.airportId}`;
		AirportDropdown.appendChild(Option);
	}
}

//webrequest for loop function
function Request()
{
	for (country of countryData) {
	webServiceRequest("https://eng1003.monash/OpenFlights/airports/",{"country":country, "callback":"AirportResponse"})
	}
}


//function that is the callback function in the initial route selection
function Routes(routes) {
	let maindiv = document.getElementById("main");
	maindiv.className = "mdl-layout__content main-map";
	maindiv.innerHTML = `<div class="active" id='map'></div>
	<div id='airport'>
		<img class='schedule-info-img' src="styles/images/baseline_info_black_24dp.png">&nbsp;<span class='schedule-info'>use the arrows to select an airport</span><br>
		&nbsp;&nbsp;<img class='schedule-arrow-img' onclick="Airport_Select(-1)" src="styles/images/baseline_arrow_left_black_24dp.png"><span id="IATA-FAA"></span><img class='schedule-arrow-img' onclick="Airport_Select(1)" src="styles/images/baseline_arrow_right_black_24dp.png"><br><br>
		<div class="airport-info">
			Name: <span id="airport-name"></span><br>
			City: <span id="airport-city"></span><br>
			Country: <span id="airport-country"></span><br>
		</div>
		<br>
		<button id="select-button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
		Select
		</button>
	</div>
	<div id='route'>
		<div>
			<div id="route-info">
			</div>
			<button id="confirm-button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
			Confirm
			</button>
		</div>
	</div>`;

	for (ap of Airports_Array) 
	{
		if (ap["airportId"] == Start[Start.length-1])
		{	
			path.push({"airport": ap});
			var origin_lnglat = [ap["longitude"], ap["latitude"]]; 
			var IATAFAA = ap["IATA-FAA"];

			document.getElementById("airport-name").innerText = ap["name"];
			document.getElementById("airport-city").innerText = ap["city"];
			document.getElementById("airport-country").innerText = ap["country"];

			break;
		}
	}

	document.getElementById("confirm-button").disabled = true;
	document.getElementById("confirm-button").classList.add("disable");
	document.getElementById("select-button").disabled = true;
	document.getElementById("select-button").classList.add("disable");

	document.getElementById("IATA-FAA").innerHTML = IATAFAA;

	map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 3,
            center: origin_lnglat
        });

	map.addControl(nav, 'top-left');

	popup = new mapboxgl.Popup({offset: popupOffsets, closeButton: false, closeOnClick: false}).setHTML(IATAFAA);

	marker = new mapboxgl.Marker({
			color: "#00ff00",
			draggable: false
		})
		.setLngLat(origin_lnglat)
		.setPopup(popup)
		.addTo(map);

	marker.togglePopup();

	line_for_map = [];

	for (route of routes)
	{
		for (ap of Airports_Array)
		{
			if (ap["airportId"] == route["destinationAirportId"])
			{
				airport_select.push({"route":route, "airport":ap});
				line_for_map.push([ap["longitude"], ap["latitude"]]);	

				marker = new mapboxgl.Marker({
					color: "#ffff00",
					draggable: false
				}).setLngLat([ap["longitude"], ap["latitude"]])
				.setPopup(new mapboxgl.Popup({offset: popupOffsets, closeButton: false, closeOnClick: false}).setHTML(ap["IATA-FAA"]))
				.addTo(map);

				//marker.togglePopup();

			}
		}
	}

	console.log(line_for_map);

	map.on('load', function () {
		for (s=0; s<line_for_map.length;  s++)
		{
			map.addSource(s.toString(10), {
				'type': 'geojson',
				'data': {
					'type': 'Feature',
					'properties': {},
					'geometry': {
						'type': 'LineString',
						'coordinates': [origin_lnglat, line_for_map[s]]
					}
				}
			});
			map.addLayer({
				'id': s.toString(10),
				'type': 'line',
				'source': s.toString(10),
				'layout': {
					'line-join': 'round',
					'line-cap': 'round'
				},
				'paint': {
					'line-color': '#888',
					'line-width': 3
				}
			});
		}
	});

	let popup_container = document.getElementsByClassName("mapboxgl-popup-content");

	for (div of popup_container)
	{
		div.className = "mapboxglpopupcontent";
	}
}

//SearchRoutes function
function SearchRoutes()
{	
	Start = document.getElementById("start").value.split(",");
	Start.push(Start[Start.length - 1].replace(" ", ""));
	Start.splice(Start.length - 2, 1);
	
	_Date = document.getElementById("date").value;
	
	
	if (Start != "" && Date != "") {
		console.log(Start);
		console.log(Date);
		
		webServiceRequest("https://eng1003.monash/OpenFlights/routes/",{"sourceAirport":Start[Start.length-1], "callback":"Routes"});
		
		return Start, Date;
	}
	else {
		alert("Please enter all the fiels!")
	}
}

Request();