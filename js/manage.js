function Delete(i)
{
	if (confirm("Are you sure you want to delete the scheduled trip?"))
	{
		trips.removeTrip(i);
		updateLocalStorage(trips);
		alert("Trip successfully deleted");
		location.reload();
	}
}

function view_trips(data)
{
	if (data.count == 0)
	{
		alert("No scheduled trips available");
		window.location = "home.html";
	}
	else
	{
		let container = document.getElementById("manage-container");
		let output = "";

		let i = 0;
		for (let obj of data.trips)
		{
			let span = "";
			for (a of obj.airports)
			{
				span += `${a.name} > `
			}
			span = span.substring(0, span.length -3);

			output += `
			<div class="manage-rectangle">
				<div class="span">
					${span}
				</div>
				<div onclick="Delete(${i})" class="delete">
				</div>
			</div>
			<br>`;

			i++;
		}
		container.innerHTML = output;
	}
}

view_trips(trips);