"use strict";

//view function
function view(i)
{
	localStorage.setItem(TRIP_INDEX_KEY, JSON.stringify(i));

	window.location = "view.html";
}

//ShowScheduledTrips function
function ShowScheduledTrips(data)
{
	/*
	*This function checks if there any trips scheduled and saved in local storage
	*If there are, for each data/schedule it will show a summarized information as in a table format
	*If there aren't any data page will show there are no scheduled trips and with a button to be directed to trip creation(schedule.html)
	*This function uses HTML Manupulation and change what the webpage(home.html) shows
	*/
	
	if (data.count != 0) //check if data(scheduled trips) are available 
	{	
		//get elements from html file for HTML DOM manipulation
		let TableHead = document.getElementById("thead");
		let TableArea = document.getElementById("tbody");
	
		//Update table header
		TableHead.innerHTML = `<div class="flights_">Scheduled flights;</div>`
		
		TableArea.innerHTML = "";

		//setting up local variables to be used in the for loop
		let i = 0;
		let info = "";
		for (let inf of data.trips) {			
			//for stop value usage
			let no_stops = "";
			if (inf.stops === 1) {
				no_stops = "1 stop";
			}
			else {
				no_stops = `${inf.stops} stops`
			}
			
			//following if, else if and else conditions are to update each trip on the page according to each trip type
			info += `
				<div class="rectangle">
					<div class="trip">Trip to ${inf.airports[inf.airports.length - 1].city} from ${inf.airports[0].city}</div> <div class="stops">${no_stops} along the way</div>
					<div class="contents">
						Flight Details:<br>
						Date <span class="date">${inf.date}</span>
					</div>
					<div class="button">
						<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary" onclick="view(${i})">
							More Flight Details
						</button>
					</div>
				</div><br>`;
			
			//update the html page
			//InfoArea.innerHTML = info;

			//incrimenting the i value used in the for loop
			i++;
		}
		TableArea.innerHTML = info
		
	}
	else { //if no trips are schedule,
		//accessing html elements with their ids
		let OutPutArea = document.getElementById("main");
		let FilterAdjust = document.getElementById("header");
		
		//changing class  list of html element with id, header
		FilterAdjust.className = "demo-layout-transparent-empty mdl-layout mdl-js-layout"

		//creating a html content
		let output = `
		<div class="empty_content">
		No Created Trips Availabe 
		<div><br>
		<div class="schedule_button">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onclick="window.location.href='schedule.html'">
			Creat Trip!
		</button>
		</div>`;
					
		//updating the html page
		OutPutArea.innerHTML = output;			
	}
}

//Runs on page load
ShowScheduledTrips(trips);