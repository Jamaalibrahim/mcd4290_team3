"use strict";
// Constants used as KEYS for LocalStorage
const TRIP_INDEX_KEY = "SelectedTripIndex";
const TRIP_DATA_KEY = "TripLocalData";
//const TEMP_TRIPS_KEY = "TempTripLocalData";
//const TEMP_TRIP_VIEW_KEY = "TempTripViewLocalData";

//Trip class
class Trip
{
	constructor(airports, route, stops, date)
	{
		this._airports = airports; //array
		this._route = route; //array
		this._stops = stops; //
		this._date = date; //string
	}
	
	//accessors
	get airports()
	{
		return (this._airports);
	}
	get route()
	{
		return (this._route);
	}
	get stops()
	{
		return (this._stops);
	}
	get date()
	{
		return (this._date);
	}
	
	//mutators
	set airports(newairports)
	{
		this._airports = newairports;
	}
	set route(newroute)
	{
		this._route = newroute;
	}
	set stops(newstops)
	{
		this._stops = newstops;
	}
	set date(newdate)
	{
		this._date = newdate;
	}
	
	fromData(data)
	{
		this._airports = data._airports;
		this._route = data._route;
		this._stops = data._stops;
		this._date = data._date;
	}
}

//Trip list class
class TripList
{
	constructor()
	{
		this._trips = [];
	}
	
	//accessors
	get trips()
	{
		return (this._trips);
	}
	get count()
	{
		return (this._trips.length);
	}
	
	//functions
	getTrip(index)
	{
		return(this._trips[index]);
	}
	addTrip(airports, route, stops, date)
	{
		let trip = new Trip(airports, route, stops, date);
		this._trips.push(trip);
	}
	removeTrip(index)
	{
		this._trips.splice(index, 1)
	}
	
	fromData(data)
	{
		this._trips = [];
		for (let object of data._trips) {
			let newTrip = new Trip();
			newTrip.fromData(object);
			this._trips.push(newTrip);
		}
			
	}
}

//function to check if local storage exist
function checkIfDataExistsLocalStorage()
{
  let data = localStorage.getItem(TRIP_DATA_KEY);
  let tt = typeof data;

  if (data != undefined && data != null && data != "")
  {
    return true;
  }
  else
  {
    return false;
  }

}

//function for updating local storage
function updateLocalStorage(data)
{
	let dataString=JSON.stringify(data);
	localStorage.setItem(TRIP_DATA_KEY, dataString);
}

//function for retrieving local storage
function getDataLocalStorage()
{
	let dataString=localStorage.getItem(TRIP_DATA_KEY);
	return (JSON.parse(dataString));
}

//global TripList instace variable
let trips = new TripList();

//run
if(checkIfDataExistsLocalStorage())
{
  let localData=getDataLocalStorage();
  trips.fromData(localData);
}